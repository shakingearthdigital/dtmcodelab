package com.shakingearthdigital.dtmtest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.huawei.hms.analytics.HiAnalytics
import com.huawei.hms.analytics.HiAnalyticsTools

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val instance = HiAnalytics.getInstance(this)
        HiAnalyticsTools.enableLog()
        val eventName = "purchase"

        val bundle = Bundle().apply {
            putDouble("price", 999.0)
            putLong("quantity", 100L)
            putString("currency", "USD")
        }

        instance?.onEvent(eventName, bundle)
    }
}
